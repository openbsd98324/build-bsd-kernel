

// ===========================================
/// BUILD KERNEL
/// cc build-kernel.c -o build-kernel
/// ./build-kernel get    netbsd 
/// ./build-kernel build  kernel  
// ===========================================


/////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////// 
/*-
 * Copyright (c) 2008 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/////////////////////////////////////////////////////// 





#include <stdio.h>
#include <string.h>
#include <stdlib.h>  //PATH_MAX in linux
#include <string.h>
#include <dirent.h>
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>
#define KRED  "\x1B[31m"
#define KGRE  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KNRM  "\x1B[0m"
#define KBGRE  "\x1B[92m"
#define KBYEL  "\x1B[93m"
int os_bsd_type = 0;
char myrepository[PATH_MAX];
char myrelease[PATH_MAX];
char myrepository_1[PATH_MAX];




int fexist(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
	/////////////////////////////
}






void nsystem( char *mycmd )
{
	printf( "<CMD: %s>\n", mycmd );
	system( mycmd );
	printf( " Defunc CMD: %s>\n", mycmd );
}











void ncmdwith( char *mycmd, char *myfile )
{
	char cmdi[PATH_MAX];
	printf( "** CMD (start) (OS: %d)\n" , MYOS );
	strncpy( cmdi , mycmd , PATH_MAX );
	strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , myfile , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
	nsystem( cmdi ); 
	printf( "** CMD (completed) (OS: %d)\n" , MYOS );
}







int ckeypress()
{
	printf( "|--------------|\n" );
	printf( "| 1: Continue  |\n" );
	printf( "|--------------|\n" );
	printf( " > Choice ?\n"      );
	int choix = getchar();
	return choix;
}		













///  /usr/pkg/bin/wget on freebsd 
void fetch_file( char *pattern )
{
	printf( "=====================\n" );
	printf( " ===  Fetch File === \n" );
	printf( "=====================\n" );
	printf( " ...\n" );
	char foocurpath[PATH_MAX]; 
	printf( "Path: %s\n", getcwd( foocurpath , PATH_MAX ) );

	if ( fexist( "/usr/bin/wget" ) == 1 )          // on linux 
	{
	     printf( " Linux wget\n" ); 
	     ncmdwith( " /usr/bin/wget  -c  " ,  pattern   );
	}

	else if ( fexist( "/usr/pkg/bin/wget" ) == 1 )          // on freebsd 
		ncmdwith( " /usr/pkg/bin/wget  -c    " ,  pattern   );


	else if ( fexist( "/usr/local/bin/wget" ) == 1 )          // on freebsd 
		ncmdwith( " /usr/local/bin/wget  -c  " ,  pattern   );


	else if ( fexist( "/usr/bin/fetch" ) == 1 )          // freebsd
		ncmdwith( " fetch -R " ,  pattern   );  // fetch -R ??


	else if ( fexist( "/etc/myname" ) == 1 )        // openbsd
		ncmdwith( " ftp " ,  pattern   );

	else if ( fexist( "/etc/wscons.conf" ) == 1 )   // netbsd
		ncmdwith( " ftp " ,  pattern   );

	else                                            // the rest
		ncmdwith( " wget -c " ,  pattern   );


	printf( " ...\n" );
	printf( "======================\n" );
}











void fetch_file_nc( char *pattern )
{
	printf( "=====================\n" );
	printf( " ===  Fetch File === \n" );
	printf( "=====================\n" );
	printf( " ...\n" );
	  ncmdwith( " wget -c --no-check-certificate  " ,  pattern   );
	printf( " ...\n" );
	printf( "======================\n" );
}









///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void pkg_set( char *fooset_url_packageserver, char *fooset_architecture, char *dirpattern , char *pattern , char *foofileextension )
{
	printf( " === ============== === \n");
	printf( " === PKG_SET MODULE === \n" ); 
	printf( " === ============== === \n");
        printf( " Arguments: %s %s %s %s %s\n", fooset_url_packageserver, fooset_architecture, dirpattern , pattern , foofileextension ); 
	printf( " === ============== === \n");

	/// dirpattern sets and pattern base.tgz
	char charo[PATH_MAX];
	char foopath[PATH_MAX];

	if ( os_bsd_type == 1)  
	{
	     printf( "FreeBSD\n" ); 
	     snprintf( charo , sizeof( charo ), "ftp://ftp.freebsd.org/pub/FreeBSD/releases/%s/12.2-RELEASE/%s.%s" , fooset_architecture , pattern, foofileextension ); 
	}

        /// mobile server, on the fly....
	/// User pass an argument to a single directory
	else if ( strcmp( fooset_url_packageserver, "" ) != 0 ) 
	{
	    printf( " NetBSD\n" ); 
            printf( " Defined pkg url...\n" ); 
	    snprintf( charo , sizeof( charo ), "%s/%s/%s.%s" , fooset_url_packageserver, fooset_architecture, pattern , foofileextension ); 
	    printf( " Server Url/File: %s\n", charo ); 
        }


	// netbsd  
	else  
	{
	        printf( " NetBSD\n" ); 
	        printf( " Else...\n" ); 
	        // netbsd 
                // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
		//snprintf( charo , sizeof( charo ), "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/%s/binary/%s/%s.%s" , fooset_architecture, dirpattern, pattern , foofileextension ); 
		snprintf( charo , sizeof( charo ), "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/%s/%s/%s.%s" , fooset_architecture, dirpattern, pattern , foofileextension ); 
	}


	printf( "Command: %s\n", charo ); 
	printf( "Path: %s\n", getcwd( foopath , PATH_MAX ) );
	fetch_file( charo ); 
}
















char *fbasename(char *name)
{
  char *base = name;
  while (*name)
    {
      if (*name++ == '/')
	{
	  base = name;
	}
    }
  return (base);
}






//////////////////////////////////////////
//////////////////////////////////////////
int main( int argc, char *argv[])
{
	int cho; int ch ; 
	int  choix; 
	int fookey;
	char cmdi[PATH_MAX];
	char cwd[PATH_MAX];
	char charo[PATH_MAX];
	char currentpath[PATH_MAX];
	char set_architecture[PATH_MAX];
	char set_extension[PATH_MAX];
	char set_url_packagesrv[PATH_MAX];

	int i; 
	FILE *fpout;

	os_bsd_type = 0; //f o n 

	if ( fexist( "/etc/freebsd-update.conf" ) == 1 )  // freebsd 
		os_bsd_type = 1;

	else if ( fexist( "/etc/myname" ) == 1 )          // openbsd
		os_bsd_type = 2;

	else if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
		os_bsd_type = 3;




	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "compile" ) ==  0 ) || ( strcmp( argv[1] , "build" ) ==  0 ) )
	if ( strcmp( argv[2] , "kernel" ) ==  0 ) 
	{
		///nsystem( "    // think you gotta build the tools first; maybe ->  " );
		///nsystem( "    printf( " you don't need xsrc to build a kernel \n ");  " );
		//printf(    "    METHOD: you will need sysinst, install 1.)base, 2.)comp and 3.)text, and/or get the source with nconfig get source.\n" ); 
		nsystem(   "    mkdir /usr/obj      /usr/tools  " );
		nsystem(   "    mkdir /usr/src/obj  /usr/src/tools  " );
		nsystem(   "    mkdir /usr/obj      /usr/tools  " );       // <-- obj is necessary
		nsystem(   "    mkdir /usr/src/obj  /usr/src/tools  " );   // <-- obj is necessary
		// needs comp and text for nbmake stuff
		nsystem(   "    echo One   ;   cd /usr/src ;  ./build.sh tools  " ); 
		nsystem(   "    echo One   ;   cd /usr/src ;  ./build.sh tools obj " ); 
		nsystem(   "    echo One   ;   cd /usr/src ;  ./build.sh obj " ); 
		nsystem(   "    echo Two   ;   cd /usr/src ;   make depend " ); 
		nsystem(   "    echo Three ;   cd /usr/src ;  ./build.sh kernel=GENERIC "   );
		return 0; 

		nsystem(   "    mkdir /usr/obj      /usr/tools  " );       // <-- obj is necessary
		nsystem(   "    mkdir /usr/src/obj  /usr/src/tools  " );   // <-- obj is necessary
		nsystem(   "    mkdir usr/obj      usr/tools  " );
		nsystem(   "    mkdir src "); 
		nsystem(   "    mkdir obj "); 
		nsystem(   "    mkdir tools "); 
		nsystem(   "    mkdir src/obj  src/tools  " );
		nsystem(   "    mkdir usr/src/obj  usr/src/tools  " );
		nsystem(   "    mkdir usr/src/obj  usr/src/tools  " );
		nsystem(   "    echo One   ;   ./build.sh tools  " ); 
		nsystem(   "    echo One   ;   ./build.sh tools obj " ); 
		nsystem(   "    echo One   ;   ./build.sh obj " ); 
		nsystem(   "    echo Two   ;   make depend " ); 
		nsystem(   "    echo Three ;   ./build.sh kernel=GENERIC "   );
		return 0; 
        }






	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	// uname  -r gives 9.1 
	// nsystem( " links ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/amd64/binary/sets/  " ); 
	////////////////////////////////////////////////////////
	if ( argc >= 3 )
	if ( strcmp( argv[1] ,     "get" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,   "sets" ) ==  0 ) || ( strcmp( argv[2] ,    "source" ) ==  0 ) 
	|| ( strcmp( argv[2] ,     "netbsd" ) ==  0 ) 
	|| ( strcmp( argv[2] ,     "comp" ) ==  0 ) 
	|| ( strcmp( argv[2] ,     "installer" ) ==  0 ) )
	{


		/// set title
		printf( "  === USAGE : GET SETS === \n" ); 
		printf( "  Usage: nconfig get sets \n" );
		printf( "  Usage: nconfig get sets http://192.168.1.100/netbsd \n" );
		printf( "  === GET SETS === \n" ); 
		printf("%s\n", KBLU);
		printf( "  Path: %s\n", getcwd( cwd , PATH_MAX ) );
		printf("%s\n", KNRM);
		if ( fexist( "/usr/bin/wget" ) == 1 )          // on freebsd 
			printf( " > WGET found on the system\n" ); 
		else if ( fexist( "/usr/pkg/bin/wget" ) == 1 )          // on freebsd 
			printf( " > WGET found on the system\n" ); 
		else if ( fexist( "/usr/local/bin/wget" ) == 1 )          // on freebsd 
			printf( " > WGET found on the system\n" ); 
		else
		{
			printf("%s", KRED);
			printf( " > WGET NOT found on the system\n" ); 
			printf("EXIT??\n");
		}


		/// set url packages
		strncpy( set_url_packagesrv, "", PATH_MAX ); 
		if ( argc == 4 )
		{
			strncpy( set_url_packagesrv, argv[ 3 ] , PATH_MAX ); 
			printf( " The URL Package Server is: %s\n ", set_url_packagesrv ); 
		}


		/// get source 
		/*
		   if  ( strcmp( argv[2] ,    "source" ) ==  0 )
		   {
		   printf( " === SOURCE === \n" ); 
		   pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "gnusrc" , "tgz" ); 
		   pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "sharesrc" , "tgz" ); 
		   pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "src" , "tgz" ); 
		   pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "syssrc" , "tgz" ); 
		   pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "xsrc" , "tgz" ); 
		   return 0; 
		   }
		 */

		/// prompt binary user selection  
		system( " uname -a "); 
		printf( "KERNEL/SETS: Select Architecture: \n" );
		printf( "1.) i386  (tgz)\n"); 
		printf( "2.) amd64 (txz or tar.xz)\n"); 
		printf( "3.) evbarm-earmv7hf (txz or tar.xz)\n"); 

		fookey = ckeypress();
		if      ( fookey == '1' ) 
			strncpy( set_architecture, "i386", PATH_MAX);
		else if ( fookey == '2' ) 
			strncpy( set_architecture, "amd64", PATH_MAX);
		else if ( fookey == '3' ) 
			strncpy( set_architecture, "evbarm-earmv7hf", PATH_MAX);
		printf( " The Package Architecture is: %s\n ", set_architecture ); 



		/// download binary user installer  (to ram), for custom use
		if  ( strcmp( argv[2] ,    "installer" ) ==  0 )
		{
			printf( " === KERNEL INSTALLER === \n" ); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/kernel", "netbsd-INSTALL" , "gz" ); 
			return 0; 
		}



		/// Get the work done...
		printf( "OS BSD Type: %d\n", os_bsd_type );
		// freebsd
		if ( os_bsd_type == 1)  // f1 o2 n3 
		{
			printf( " The OS Type is: FreeBSD\n"); 
			strncpy( set_extension   , "txz", PATH_MAX);

			pkg_set( set_url_packagesrv, set_architecture, "sets",  "base", set_extension ); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "doc", set_extension ); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "kernel-dbg" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "kernel", set_extension ); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "lib32-dbg" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "lib32" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "ports", set_extension ); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "src" , set_extension); 
			// test or tests ... 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "tests" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "sets",  "test" , set_extension); 

		}


		// openbsd
		else if ( os_bsd_type == 2)  // f1 o2 n3 
		{
			printf( " The OS Type is: OpenBSD\n"); 
		}

		// netbsd
		else
		{
			printf( " The OS Type is: NetBSD \n"); 
			if ( strcmp( set_architecture, "amd64" ) == 0 ) 
				strncpy(  set_extension   , "tar.xz", PATH_MAX);
			if ( strcmp( set_architecture, "i386" ) == 0 ) 
				strncpy(  set_extension   , "tgz", PATH_MAX);
			else if ( fookey == '3' ) 
				strncpy(  set_extension   , "tgz", PATH_MAX);
			printf( " The Package Architecture is: %s\n ", set_extension ); 


			// ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
			//  gnusrc  sharesrc   src   syssrc   xsrc     
			printf( " === ====== === \n" ); 
			printf( " === SOURCE === \n" ); 
			printf( " === ====== === \n" ); 
			if ( argc == 4 )
			{
				pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "gnusrc" , "tgz"); 
				pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "sharesrc" , "tgz"); 
				pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "src" , "tgz"); 
				pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "syssrc" , "tgz"); 
				pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xsrc" , "tgz"); 
			}



			// netbsd source
			if ( ( strcmp( argv[2] ,   "source" )  ==  0 ) || ( strcmp( argv[2] ,   "netbsd" )  ==  0 ) )
			{
				pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "gnusrc" , "tgz" ); 
				pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "sharesrc" , "tgz" ); 
				pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "src" , "tgz" ); 
				pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "syssrc" , "tgz" ); 
				pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "xsrc" , "tgz" ); 
			        if ( strcmp( argv[2] ,   "source" )  ==  0 ) 
				{
				  printf( "Only source requested.\n"); 
				  return 0; 
				}
			}





			// netbsd comp sets, only
			if ( strcmp( argv[2] ,     "comp" ) ==  0 ) 
			{
				printf( " === COMP SOURCE FILES === \n" ); 
				// pkg_set( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", "source/sets", "sets", "gnusrc" , "tgz" ); 
				// snprintf( charo , sizeof( charo ), "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/%s/%s/%s.%s" , fooset_architecture, dirpattern, pattern , foofileextension ); 
				//strncpy( set_url_packagesrv, "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1", PATH_MAX );  
				pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "comp" , set_extension); 
				pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "text" , set_extension); 
				return 0; 
			}


			// netbsd kernel
			printf( " === ====== === \n" ); 
			printf( " === ====== === =================== \n" ); 
			printf( " === KERNEL GENERIC AND INSTALL === \n" ); 
			printf( " === ====== === =================== \n" ); 
			printf( " === ====== === \n" ); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/kernel", "netbsd-INSTALL" , "gz" ); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/kernel", "netbsd-GENERIC" , "gz" ); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/kernel", "netbsd-LEGACY" ,  "gz" ); 

			// netbsd sets
			printf( " === SETS === \n" ); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/sets", "base" , set_extension); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/sets", "comp" , set_extension); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/sets", "etc" ,  set_extension); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/sets", "debug" ,  set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "kern-GENERIC", set_extension ); 
			pkg_set( set_url_packagesrv,  set_architecture, "binary/sets", "games" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "kern-GENERIC" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "man" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "modules" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "misc" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "rescue" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "test" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "tests" , set_extension);  // <-- this one !! not test 
			pkg_set( set_url_packagesrv, set_architecture,  "binary/sets", "text" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xserver" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xbase" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xcomp" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xdebug" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xetc" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xfont" , set_extension); 
			pkg_set( set_url_packagesrv, set_architecture, "binary/sets", "xserver" , set_extension); 



		}

		printf("%d\n",   (int)time(NULL)   );
		printf( " == DEBUG Content File ==\n" ); 
		snprintf( charo , sizeof( charo ), " echo \"unixtime: %d\" >> sets-content-%s " ,   (int)time(NULL)  ,  set_architecture );
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0; 
        }












/*
 Command:   progress -zf /targetroot/usr/INSTALL/kern-GENERIC.tgz   tar --chroot -xpf - './netbsd.img'                                                      
 Command: progress -zf /usr/INSTALL/xcomp.tgz   tar  --chroot -xpf -       
 $ sudo dd if=~/Downloads/rpi.img of=/dev/mmcblk0 bs=2M status=progress
*/
	////////////////////////////////////////////////////////
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "unpack" ) ==  0 )
	||  ( strcmp( argv[1] , "-unpack" ) ==  0 )
	||  ( strcmp( argv[1] , "--unpack" ) ==  0 )   )
	if ( MYOS != 1 ) 
	if ( os_bsd_type == 3 ) //netbsd for progress
	{
			// mini -args 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );
					}
				}
			}
			////

			/////////////////////////
			// run... 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						strncpy( cmdi, "", PATH_MAX );
						strncat( cmdi , "   progress -zf   " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "\"    tar  --chroot -xpf -     " , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );
						printf( "- File process (status: completed).\n" );
					}
				}
			}
			return 0;
		}









	/////////////////
	printf("Util: Bye!\n");
	return 0;
}







